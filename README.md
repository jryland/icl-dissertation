# README #

### Purpose ###

* This runs several demonstrations of the ICL model developed for the dissertation of James Ryland
* Version 1.0.0

### Setup in Google Colab: ###
* Download files
* Place them in a google drive folder (probably in their own folder)
* open files in google drive and follow the instructions in the comments (usually the demonstration comments are at the end)
* when running in google colab dependencies should already be taken care of or will be downloaded auto-magically (actually I put code in there to do it...)

### Notes ###
* Currently google colab 2X uses tensorflow 2.4, this may change in the future and additional code will need to be added to force the colab enviroment to download tensorflow 2.4. This is a relatively easy fix.
* I do appologize for the need to uncomment run commands to use some of these files and the lack of commenting in certain sections. 
* Also the code looks like aboslute giberish in the bitbucked file viewer... you will need a python editor and environment with Jupyter Notebook editing capabilities to view it successfully outside of google colab

# Files
* ArborLayer: contains demonstrations for the arbor layer model of axonal development
* SimpleICLayer: contains demonstrations for the simple only version of the ICL model
* SimpleComplexICLayer: 
* EX1PerLayer_MNISTF: contains demonstrations for Simulation Study 3 in the disertation
* EX1PerLayer_IMAGENETTE_SimpleOnly: contains demonstrations for Simulation Study 3 in the disertation
* Exp123_FMNIST_SimpleOnly: Contains demonstrations for Simulation Studies 2 and 3, and a poorly tuned version of Simulation Study 1. 


### Who do I talk to? ###
* James Ryland 
* Email 1: jwr071000@utdallas.edu
* Email 2: james.w.ryland@gmail.com